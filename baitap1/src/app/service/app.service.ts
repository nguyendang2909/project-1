import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  readonly API_URL = 'http://localhost:1234/';
  constructor(private http: HttpClient) { }

  connectToNode(){
    return this.http.get(this.API_URL);
  }
  postData(data){
    return this.http.post(`${this.API_URL}postData`,data);
  }
}
