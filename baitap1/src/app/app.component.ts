import { Component } from '@angular/core';
import { FormControl, EmailValidator} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { getLocaleDateFormat } from '@angular/common';
import { AppService } from './service/app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  a = true;
  b = true;
  message = false;
  textmessage = '';
  loiemailpassword = false;
  textloiemailpassword = '';
  loipassword = false;
  textloipassword = '';
  loiemail = false;
  textloiemail = '';
   title = 'Angular';
  title2 = '3 authors';
  text = new FormControl('');
  constructor(private appService: AppService){

  }
  show() {
    this.a = !this.a;
  }

  course2() {
    this.b = !this.b;
  }
  dangnhap(form) {

    if(form.value.email.length == 0 || form.value.password.length == 0) {this.loiemailpassword = false;
      this.textloiemailpassword='You must provide both email and password'}
    else {this.loiemailpassword = true; this.textloiemailpassword='';
  
    if (form.value.email.indexOf('@')==-1 || form.value.email.indexOf('@')==0 || form.value.email.indexOf('@')==form.value.email.length-1)
    {this.loiemail = false;
      this.textloiemail = 'Invalid email';
    }
    else {this.loiemail = true, this.textloiemail =''}



    if(form.value.password.length > 0 && form.value.password.length < 6
      || form.value.password.match(/[a-z]/)==null 
      || form.value.password.match(/[A-Z]/)==null 
      || form.value.password.match(/[0-9]/)==null)
    {this.loipassword = false;
      this.textloipassword = 'Invalid password'
    }
    else {this.loipassword =true; this.textloipassword = ''}


  };
           
   
    if (this.loiemailpassword == false || this.loipassword == false || this.loiemail ==false ) {this.message = false;
      this.textmessage = this.textloiemailpassword + this.textloipassword + this.textloiemail;
      }
      else {this.message = true;
      this.textmessage = 'All good';
      };
  }


  getData(){
    return this.appService.connectToNode().subscribe((data)=> console.log(data));
  }

  postData(data){
    return this.appService.postData(data).subscribe((res) => console.log(res));
  }
}





